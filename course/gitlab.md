---
title: Introduction to GitLab
nav_order: 1
parent: Introduction
---

## GitLab 

GitLab is The DevOps platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in within an organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability.

GitLab is an open core company which develops software for the software development lifecycle with 30 million estimated registered users and more than 1 million active license users, and has an active community of more than 2,500 contributors. GitLab openly shares more information than most companies and is public by default, meaning our projects, strategy, direction and metrics are discussed openly and can be found within our website. Our values are Collaboration, Results, Efficiency, Diversity, Inclusion & Belonging , Iteration, and Transparency ([CREDIT](https://about.gitlab.com/handbook/values/#credit)) and these form our culture.

GitLab's mission is to make it so that **everyone can contribute**

## The DevOps Platform 

GitLab is a single platform that provides features across the DevOps lifecycle. Learn more about [GitLab's features for each stage in the DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). [This video](https://about.gitlab.com/why-gitlab/) provides an overview of GitLab's features. 

GitLab has customers across the Globe in every sector in a wide array of verticals including transportation, financial services, public sector, consumer/retail, and technology. Learn more about the different [solutions](https://about.gitlab.com/solutions/) GitLab offers. 

See our [case studies](https://about.gitlab.com/customers/) for specific examples of how our Customers use GitLab. 

## GitLab in Education 

GitLab is also used extensive in with the education market both within academic, K-12, and related sectors (consulting, learning, etc). 

The use cases include: 

### Open Science
* Git technology to manage data and publications
* Continuous integration for analytical workflows on data
* Containerization for reproducibility and versioned analysis
* Collaboration - speeding up the cycle to results
* Open web publications

### Learning
* From basics of coding to building applications and systems
* Students use GitLab to learn the stages of DevOps Lifecycle
* Students store code and professors use CI to check quality/grade
* Collaboration for group projects

### Student Portfolios
* Showcase code base
* Showcase contributions
* Showcase collaborations
* Host and publish blogs
* Credibility as for skills in DevOps

See specific examples in Education Case Studies
* [The University of Surrey achieves top marks for collaboration and workflow management with GitLab](https://about.gitlab.com/customers/university-of-surrey/)
* [How Dublin City University empowers students for the IT industry with GitLab](https://about.gitlab.com/customers/dublin-city-university/)
* [GitLab advances open science education at Te Herenga Waka – Victoria University of Wellington](https://about.gitlab.com/customers/victoria_university/)

## Practical Examples

The focus of this workshop is on practical examples of using GitLab in the classroom. We showcase two main examples. 

## CS 490, Western New England University 
Instructor, Heidi Ellis uses GitLab to host the course content for CS 490. This is a simple example of how GitLab can be used as a source control management system to serve up content to students. Heidi uses GitLab for:
* Using GitLab Project as a single source of truth for course
* Syllabus, schedule, homework files, slides, and more all stored in directories and files in repo
* Open Source as a default for work

## Advanced Software Engineering, Morehouse College
GitLab team members collaborate to teach an Advanced Software Engineering course at Morehouse College. The course is lead by Darva Satcher, Director of Engineering, Create, at GitLab. Darva uses GitLab both for the course content (syllabus, schedule,  etc) as well host o 
* [Static site](https://morehouse-dcherry.gitlab.io/advanced-software-engineering/lectures/) 
* [Course content source repository](https://gitlab.com/gitlab-com/marketing/community-relations/education-program/advanced-software-engineering-template)
* [Technical Projects](https://gitlab.com/gitlab-ase) 

### Other resources 
*[Paul G Allen School of Computer Science - GitLab Guide](https://www.cs.washington.edu/lab/gitlab)


[Next Page](https://devops-education.gitlab.io//workshops/practical-classroom-workshop/course/getting-started/)