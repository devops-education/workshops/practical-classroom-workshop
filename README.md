# Practical Classroom Workshop
This repo hosts files and the static website for a practical workshop on using GitLab in the classroom. Please see the instructions in the slide deck and static website this repo creates. 

[Slidedeck](https://gitlab.com/devops-education/workshops/practical-classroom-workshop/-/blob/main/EDSIGCON%20Nov%2022%20Presentation.pdf)

[Link to website](https://devops-education.gitlab.io/workshops/practical-classroom-workshop/)

[8-ball python code for workshop](https://gitlab.com/devops-education/workshops/practical-classroom-workshop/-/blob/main/sample_code.txt)


Thank you! 
